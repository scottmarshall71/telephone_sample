package com.dizzyjock.aconex.telephone.main;

import org.testng.annotations.Test;

/**
 * AppMain calls System.exit(0), so dont run these tests as normal
 * 
 * @author ubuntu
 * 
 */
public class AppMainTest
{

	// AppMain calls System.exit(0), so dont run these tests as normal
	@Test(enabled = false)
	public void should_match_and_generate_file_for_scholar()
	{
		// should match 'scholar'
		final AppMain appMain = new AppMain();
		appMain.main(new String[]
		{ "-n", "724.6527" });
	}

	@Test(enabled = false)
	public void should_pick_up_alt_dictionary_from_absolute_path()
	{
		// should match 'scholar'
		final AppMain appMain = new AppMain();
		appMain.main(new String[]
		{ "-d", "/home/ubuntu/projects/telephone/alt.dictionary", "-n", "724.6527" });
	}

	@Test(enabled = false)
	public void should_pick_up_alt_dictionary_from_same_dir()
	{
		// should match 'scholar'
		final AppMain appMain = new AppMain();
		appMain.main(new String[]
		{ "-d", "alt.dictionary", "-n", "724.6527" });
	}

	// AppMain calls System.exit(0), so dont run these tests as normal
	@Test(enabled = false)
	public void should_match_and_generate_file_for_other()
	{
		// should match 'scholar'
		final AppMain appMain = new AppMain();
		appMain.main(new String[]
		{ "-n", "68437" });
	}

	// AppMain calls System.exit(0), so dont run these tests as normal
	@Test(enabled = false)
	public void should_match_and_generate_file_for_testing()
	{
		// should match 'testing'
		final AppMain appMain = new AppMain();
		appMain.main(new String[]
		{ "-n", "8378464" });
	}

	// AppMain calls System.exit(0), so dont run these tests as normal
	@Test(enabled = false)
	public void should_not_match_and_should_not_generate_file()
	{
		// should match 'scholar'
		final AppMain appMain = new AppMain();
		appMain.main(new String[]
		{ "-n", "111111" });
	}
}
