package com.dizzyjock.aconex.telephone.data;

import org.testng.annotations.Test;

/**
 * Not really a test, but wanted to verify the init was called only once.
 * 
 * @author ubuntu
 * 
 */
public class NumberMapperInitTest
{
	@Test
	public void testInit()
	{
		new NumberMapper();
		System.out.println("Testing after 1");
		new NumberMapper();
		System.out.println("Testing after 2");
		new NumberMapper();
	}

}
