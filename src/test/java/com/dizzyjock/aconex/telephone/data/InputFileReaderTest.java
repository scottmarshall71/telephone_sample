package com.dizzyjock.aconex.telephone.data;

import java.io.FileNotFoundException;
import java.util.List;

import junit.framework.Assert;

import org.testng.annotations.Test;

import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;

public class InputFileReaderTest
{

	// this needs to be disabled by default as it relies on the absolute path
	@Test(enabled = false)
	public void should_read_file_from_location() throws FileNotFoundException
	{
		final List<TelephoneNumber> telephoneNumbers = new InputFileReaderImpl()
				.readFile("/home/ubuntu/projects/telephone/src/test/resources/inputreadertest/inputFileReaderTest.txt");
		Assert.assertEquals(1, telephoneNumbers.size());
		Assert.assertEquals(telephoneNumbers.get(0), new TelephoneNumber("12345"));
	}

	@Test(enabled = true, expectedExceptions = FileNotFoundException.class)
	public void should_error_when_no_file() throws FileNotFoundException
	{
		new InputFileReaderImpl()
				.readFile("/inputFileReaderTest.txt");

	}

}
