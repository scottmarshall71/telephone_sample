package com.dizzyjock.aconex.telephone.data;

import java.util.Map;
import java.util.Set;

import com.dizzyjock.aconex.telephone.domain.Result;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberKey;

/**
 * Test implementation that writes to System.out instead of the file system.
 * 
 * @author ubuntu
 * 
 */
public class OutputFileWriterConsoleImpl implements OutputFileWriter
{
	private static final String CLASSNAME = OutputFileWriterConsoleImpl.class.getSimpleName();

	@Override
	public void writeFile(final Result result)
	{

		if (result.getResultMap().isEmpty())
		{
			System.out.println("No matches found");
		}

		else
		{
			for (final Map.Entry<TelephoneNumberKey, Set<String>> mapEntry : result.getResultMap().entrySet())
			{
				for (final String match : mapEntry.getValue())
				{
					System.out.printf("[%s]:[%s] TelephoneNumberKey:[%s] - Match:[%s]%n",
							Thread.currentThread().getName(),
							CLASSNAME, mapEntry.getKey().getIdentifier(), match);

				}
				System.out.printf("[%s]:[%s] OutputFileWriter ending%n", Thread.currentThread().getName(),
						CLASSNAME);
			}
		}
	}
}
