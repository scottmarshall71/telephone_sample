package com.dizzyjock.aconex.telephone.data;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.dizzyjock.aconex.telephone.processor.TelephoneProcessConstants;

/**
 * Class to test picking up the dictionary from default or command line
 * 
 * @author ubuntu
 * 
 */
public class DictionaryTest
{

	@Test(enabled = true)
	public void should_read_default_dictionary() throws IOException
	{
		final DictionaryFactory dictionaryFactory = new DictionaryFactory(TelephoneProcessConstants.DEFAULT_DICTIONARY);
		final Dictionary dictionary = dictionaryFactory.createDictionary();
		Assert.assertEquals(dictionary.getDictionarySrc(), TelephoneProcessConstants.DEFAULT_DICTIONARY);
		Assert.assertNotNull(dictionary.readLine());
	}

	@Test(enabled = true)
	public void should_read_dictionary_from_relative_path() throws IOException
	{
		final DictionaryFactory dictionaryFactory = new DictionaryFactory("alt.dictionary");
		final Dictionary dictionary = dictionaryFactory.createDictionary();
		Assert.assertEquals(dictionary.getDictionarySrc(), "alt.dictionary");
		Assert.assertNotNull(dictionary.readLine());
	}

	// uses absolute path : leave disabled as the norm
	@Test(enabled = false)
	public void should_read_dictionary_from_absolute_path() throws IOException
	{
		final DictionaryFactory dictionaryFactory = new DictionaryFactory(
				"/home/ubuntu/projects/telephone/alt.dictionary");
		final Dictionary dictionary = dictionaryFactory.createDictionary();
		Assert.assertEquals(dictionary.getDictionarySrc(), "/home/ubuntu/projects/telephone/alt.dictionary");
		Assert.assertNotNull(dictionary.readLine());
	}
}
