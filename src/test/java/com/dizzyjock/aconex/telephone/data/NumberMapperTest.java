package com.dizzyjock.aconex.telephone.data;

import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class NumberMapperTest
{

	@Test(enabled = true, dataProvider = "data")
	public void shoud_return_expectedValues(
			final MappedNumberEnum mappedNumber,
			final Character[] expectedValues)
	{
		final Set<Character> mappedValues = new NumberMapper()
				.lookup(mappedNumber);
		for (final Character expectedValue : expectedValues)
		{
			Assert.assertTrue(mappedValues.contains(expectedValue));
		}

	}

	@DataProvider(name = "data")
	public Object[][] provide_input_and_output_values()
	{
		return new Object[][]
		{
				{ MappedNumberEnum.TWO, new Character[]
				{ 'a', 'b', 'c' } },
				{ MappedNumberEnum.THREE, new Character[]
				{ 'd', 'e', 'f' } },
				{ MappedNumberEnum.FOUR, new Character[]
				{ 'g', 'h', 'i' } },
				{ MappedNumberEnum.FIVE, new Character[]
				{ 'j', 'k', 'l' } },
				{ MappedNumberEnum.SIX, new Character[]
				{ 'm', 'n', 'o' } },
				{ MappedNumberEnum.SEVEN,
						new Character[]
						{ 'p', 'q', 'r', 's' } },
				{ MappedNumberEnum.EIGHT, new Character[]
				{ 't', 'u', 'v' } },
				{ MappedNumberEnum.NINE, new Character[]
				{ 'w', 'x', 'y', 'z' } }

		};
	}

	@Test(enabled = true)
	public void shoud_return_empty_set()
	{
		final Set<Character> mappedValues = new NumberMapper()
				.lookup(MappedNumberEnum.NOTMAPPED);
		Assert.assertTrue(mappedValues.isEmpty());

	}

}
