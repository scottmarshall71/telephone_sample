package com.dizzyjock.aconex.telephone.util;

import org.testng.annotations.Test;

public class UncaughtExceptionHandlerTest
{

	@Test
	public void displayStackTrace()
	{
		final UncaughtExceptionHandler handler = new UncaughtExceptionHandler();
		handler.uncaughtException(Thread.currentThread(), new RuntimeException("Planned exception,part of test suite"));

	}

}
