package com.dizzyjock.aconex.telephone.processor;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.InputFileReader;
import com.dizzyjock.aconex.telephone.domain.CommandLineInputData;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.exception.NoArgException;

public class CommandLineParserTest
{

	private CommandLineParserTestFixture testFixture;

	@BeforeMethod
	public void setup()
	{
		this.testFixture = new CommandLineParserTestFixture();
	}

	@Test(enabled = true)
	public void should_recognise_command_line_dictionary_arg() throws NoArgException, FileNotFoundException
	{
		testFixture.given_dictionary_as_arg("alt.dictionary");
		testFixture.when_args_processed();
		testFixture.the_verify_user_dictionary_present();
	}

	@Test(enabled = true, expectedExceptions = FileNotFoundException.class)
	public void should_fail_if_no_dictionary_arg_passed() throws NoArgException, FileNotFoundException
	{
		testFixture.given_invalid_dictionary_args();
		testFixture.when_args_processed();
		testFixture.the_verify_user_dictionary_present();
	}

	@Test(enabled = true, expectedExceptions = FileNotFoundException.class)
	public void should_fail_if_empty_dictionary_arg_passed() throws NoArgException, FileNotFoundException
	{
		testFixture.given_dictionary_as_arg("");
		testFixture.when_args_processed();
		testFixture.the_verify_user_dictionary_present();
	}

	@Test(enabled = true)
	public void should_use_default_when_no_dictionary_arg() throws NoArgException, FileNotFoundException
	{
		testFixture.given_no_dictionary_args();
		testFixture.when_args_processed();
		testFixture.then_verify_default_dictionary_present();
	}

	@Test(enabled = true)
	public void should_recognise_command_line_numbers() throws NoArgException, FileNotFoundException
	{
		testFixture.given_args(new String[]
		{ "-n", "12345", "67890" });
		testFixture.when_args_processed();
		testFixture.then_verify_numbers_present(new TelephoneNumber("67890"),
				new TelephoneNumber("12345"));
	}

	@Test(enabled = true)
	public void should_break_numbers_on_other_option() throws NoArgException, FileNotFoundException
	{
		testFixture.given_args(new String[]
		{ "-n", "12345", "67890", "-d", "alt.dictionary" });
		testFixture.when_args_processed();
		testFixture.then_verify_numbers_present(new TelephoneNumber("67890"),
				new TelephoneNumber("12345"));
	}

	/**
	 * The test implementation of InputFileReader returns two fixed values each
	 * time it is called
	 * 
	 * @throws NoArgException
	 */
	@Test(enabled = true)
	public void should_recognise_command_line_files() throws NoArgException, FileNotFoundException
	{
		testFixture.given_args(new String[]
		{ "-f", "fixed_values",
				"fixed_values" });
		testFixture.when_args_processed();
		testFixture.then_verify_numbers_present(new TelephoneNumber("67890"),
				new TelephoneNumber("67890"), new TelephoneNumber("12345"), new TelephoneNumber("12345"));
	}

	/**
	 * The test implementation of InputFileReader returns two fixed values each
	 * time it is called
	 * 
	 * @throws NoArgException
	 */
	@Test(enabled = true)
	public void should_recognise_other_command_line_options() throws NoArgException, FileNotFoundException
	{
		testFixture.given_args(new String[]
		{ "-f", "fixed_values",
				"fixed_values",
				"-d", "alt.dictionary" });
		testFixture.when_args_processed();
		testFixture.then_verify_numbers_present(new TelephoneNumber("67890"),
				new TelephoneNumber("67890"), new TelephoneNumber("12345"), new TelephoneNumber("12345"));
	}

	@Test(enabled = true, expectedExceptions = FileNotFoundException.class)
	public void should_error_if_no_files_supplied() throws NoArgException, FileNotFoundException
	{
		testFixture.given_args(new String[]
		{ "-f", "-d", "dummy.txt" });
		testFixture.when_args_processed();
	}

	@Test(enabled = true)
	public void should_throw_exception_if_no_file_or_number() throws FileNotFoundException
	{
		testFixture.given_args(new String[]
		{ "-d", "alt.dictionary" });
		try
		{
			testFixture.when_args_processed();
			Assert.fail("Should have thrown NoArgException");
		}
		catch (final NoArgException e)
		{
			// expected
			Assert.assertTrue(true);
		}
	}

	private class CommandLineParserTestFixture
	{

		private final CommandLineParser commandLineParser;
		private String[] args;
		private CommandLineInputData commandLineInputData;
		private String userDictionary;

		public CommandLineParserTestFixture()
		{
			this.commandLineParser = new CommandLineParser(new InputFileReaderTestImpl("12345", "67890"));
		}

		public void given_dictionary_as_arg(final String userDictionary)
		{
			this.userDictionary = userDictionary;
			args = new String[]
			{ "-d", userDictionary, "-n", "12345" };
		}

		public void given_invalid_dictionary_args()
		{
			args = new String[]
			{ "-d" };
		}

		public void given_no_dictionary_args()
		{
			args = new String[]
			{ "-n", "123456" };
		}

		public void given_args(final String[] args)
		{
			this.args = args;
		}

		public void when_args_processed() throws NoArgException, FileNotFoundException
		{
			commandLineInputData = commandLineParser.parseCommandLine(args);
		}

		public void the_verify_user_dictionary_present()
		{
			final DictionaryFactory dictionaryFactory = commandLineInputData.getDictionaryFactory();
			Assert.assertEquals(dictionaryFactory.getDictionarySrc(), userDictionary);
		}

		public void then_verify_default_dictionary_present()
		{
			final DictionaryFactory dictionaryFactory = commandLineInputData.getDictionaryFactory();
			Assert.assertEquals(dictionaryFactory.getDictionarySrc(), TelephoneProcessConstants.DEFAULT_DICTIONARY);
		}

		public void then_verify_numbers_present(final TelephoneNumber... telephoneNumbers)
		{

			Assert.assertEquals(commandLineInputData.getTelephoneNumbers().size(), telephoneNumbers.length);
			for (final TelephoneNumber telephoneNumber : telephoneNumbers)
			{
				Assert.assertTrue(commandLineInputData.getTelephoneNumbers().contains(telephoneNumber));
			}

		}
	}

	/*
	 * Test implementation that returns two telephone numbers every time it is
	 * invoked
	 */
	private class InputFileReaderTestImpl implements InputFileReader
	{

		private final List<TelephoneNumber> telephoneNumbers;

		public InputFileReaderTestImpl(final String... args)
		{
			telephoneNumbers = new ArrayList<>();
			for (final String arg : args)
			{
				telephoneNumbers.add(new TelephoneNumber(arg));
			}
		}

		@Override
		public List<TelephoneNumber> readFile(final String absolutePathToFile) throws FileNotFoundException
		{
			return telephoneNumbers;

		}

	}

}
