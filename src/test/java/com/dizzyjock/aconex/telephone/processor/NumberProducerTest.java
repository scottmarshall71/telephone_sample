package com.dizzyjock.aconex.telephone.processor;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.NumberMapper;
import com.dizzyjock.aconex.telephone.data.OutputFileWriterConsoleImpl;
import com.dizzyjock.aconex.telephone.domain.CommandLineInputData;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.util.CharacterFilter;

/**
 * This class tests the initial processing of the input numbers
 * 
 * @author ubuntu
 * 
 */
public class NumberProducerTest
{
	private static final String CLASSNAME = NumberProducerTest.class.getSimpleName();
	private NumberProducerTestFixture testFixture;

	@BeforeMethod
	public void setup()
	{
		this.testFixture = new NumberProducerTestFixture();
	}

	@Test(enabled = true)
	public void should_start_number_producer() throws InterruptedException, FileNotFoundException
	{
		testFixture.given_dictionary("default.txt");
		testFixture.when_producer_started();
	}

	private class NumberProducerTestFixture
	{

		private NumberProducer numberProducer;

		/**
		 * @param filename
		 * @throws FileNotFoundException
		 */
		public void given_dictionary(final String filename) throws FileNotFoundException
		{
			final String fileSeparator = System.getProperty("file.separator");
			final String pathToDictionary = String.format("numberproducertest%s%s",
					fileSeparator, filename);
			final DictionaryFactory dictionaryFactory = new DictionaryFactory(pathToDictionary);
			final List<TelephoneNumber> telephoneNumbers = new ArrayList<>();
			telephoneNumbers.add(new TelephoneNumber("1234567"));
			final CommandLineInputData commandLineInputData = new CommandLineInputData(dictionaryFactory,
					telephoneNumbers);
			numberProducer = new NumberProducer(commandLineInputData,
					new NumberMapper(),
					new CharacterFilter(),
					new OutputFileWriterConsoleImpl());
		}

		public void when_producer_started()
		{
			numberProducer.produceNumbers();

		}

	}

}
