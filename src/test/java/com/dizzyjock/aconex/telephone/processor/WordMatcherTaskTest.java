package com.dizzyjock.aconex.telephone.processor;

import java.io.FileNotFoundException;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.NumberMapper;
import com.dizzyjock.aconex.telephone.domain.Result;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberData;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberKey;
import com.dizzyjock.aconex.telephone.util.CharacterFilter;

/**
 * This class tests passing a known TelephoneNumber against a specified
 * dictionary of 1 or more words to verify results.
 * 
 * @author ubuntu
 * 
 */
public class WordMatcherTaskTest
{

	private WordMatcherTaskTestFixture testFixture;

	@BeforeMethod
	public void setup()
	{
		this.testFixture = new WordMatcherTaskTestFixture();
	}

	@Test(enabled = true)
	public void should_match_exact_length() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("8378", "test",
				"should_match_exact_length.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_not_put_number_back_at_end_of_dictionary() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("9999", "test",
				"should_not_put_number_back_at_end_of_dictionary.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_no_match_is_made();
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_match_single_wildcard() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("8978", "test",
				"should_match_single_wildcard.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("t9st");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_not_match_double_wildcard() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("8998", "test",
				"should_not_match_double_wildcard.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_no_match_is_made();
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_not_match_when_word_is_longer_than_number() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("8378", "testing_long_word",
				"should_not_match_when_word_is_longer_than_number.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_no_match_is_made();
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_add_excess_numbers_back_to_queue() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("83781111", "test",
				"should_add_excess_numbers_back_to_queue.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test");
		testFixture.then_verify_queue(new TelephoneNumber("1111"));
	}

	@Test(enabled = true)
	public void should_add_excess_numbers_back_to_queue_for_last_char() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("83781", "test",
				"should_add_excess_numbers_back_to_queue_for_last_char.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test");
		testFixture.then_verify_queue(new TelephoneNumber("1"));
	}

	@Test(enabled = true)
	public void should_replace_unmapped_numbers_with_wildcard() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("8178", "test",
				"should_replace_unmapped_numbers_with_wildcard.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("t1st");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_ignore_punctuation_in_telephone() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("83.78", "test",
				"should_match_exact_length.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_ignore_whitespace_in_telephone() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("83 78", "test",
				"should_match_exact_length.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_ignore_punctuation_in_word() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("8378", "te.st",
				"should_match_exact_length.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_ignore_whitespace_in_word() throws FileNotFoundException
	{
		testFixture.given_telephone_number_and_word_and_dictionary("8378", "te st",
				"should_match_exact_length.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test");
		testFixture.then_verify_queue_is_empty();
	}

	private class WordMatcherTaskTestFixture
	{

		private WordMatcherTask wordMatcherTask;

		private final Result result;
		private final BlockingQueue<TelephoneNumberData> numbersQueue;
		private TelephoneNumber telephoneNumber;
		private final TelephoneNumberKey key = new TelephoneNumberKey("2222");

		public WordMatcherTaskTestFixture()
		{
			result = new Result();
			numbersQueue = new ArrayBlockingQueue<>(TelephoneProcessConstants.INITIAL_QUEUE_CAPACITY);

		}

		public void given_telephone_number_and_word_and_dictionary
				(final String telNumber, final String word, final String filename) throws FileNotFoundException
		{
			final String fileSeparator = System.getProperty("file.separator");
			final String pathToDictionary = String.format("wordmatchertasktest%s%s",
					fileSeparator, filename);

			telephoneNumber = new TelephoneNumber(telNumber);
			final TelephoneNumberData telephoneNumberData = new TelephoneNumberData(key, telephoneNumber);
			this.wordMatcherTask = new WordMatcherTask(
					new DictionaryFactory(pathToDictionary),
					telephoneNumberData,
					new NumberMapper(),
					numbersQueue,
					result,
					new CharacterFilter());
		}

		public void when_match_is_run()
		{
			try
			{
				wordMatcherTask.call();
			}
			catch (final FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				Assert.fail(e.getMessage());
			}
		}

		public void then_verify_match_is_made(final String expectedResult)
		{
			final Result actualResult = wordMatcherTask.getResult();
			final Set<String> matches = actualResult.getResultMap().get(key);
			Assert.assertTrue(matches.contains(expectedResult));
			// final Iterator<String> it =
			// actualResult.getResultMap().get(telephoneNumber).iterator();
			// Assert.assertEquals(it.next(), expectedResult);
			System.out.println("Actual " + actualResult);
		}

		public void then_verify_no_match_is_made()
		{
			final Result actualResult = wordMatcherTask.getResult();
			Assert.assertTrue(actualResult.getResultMap().isEmpty());
		}

		public void then_verify_queue(final TelephoneNumber expectedResult)
		{
			displayQueueContents();
			verifyPoisonPresent();
			removePoison();
			Assert.assertEquals(1, wordMatcherTask.getNumbersQueue().size());
			final TelephoneNumberData actualResult = wordMatcherTask.getNumbersQueue().poll();
			Assert.assertEquals(actualResult.getTeleponeNumber(), expectedResult);
			Assert.assertTrue(wordMatcherTask.getNumbersQueue().isEmpty());
		}

		public void then_verify_queue_is_empty()
		{
			displayQueueContents();
			removePoison();
			Assert.assertEquals(0, wordMatcherTask.getNumbersQueue().size());
		}

		private void removePoison()
		{
			for (final TelephoneNumberData queueElement : numbersQueue)
			{
				if (queueElement.getTeleponeNumber().equals(TelephoneProcessConstants.POISON))
				{
					numbersQueue.remove(queueElement);
				}
			}
		}

		private void verifyPoisonPresent()
		{
			final TelephoneNumberData poisonData = new TelephoneNumberData(key, TelephoneProcessConstants.POISON);
			Assert.assertTrue(numbersQueue.contains(poisonData));
		}

		private void displayQueueContents()
		{
			final Object[] remainingNumbers = wordMatcherTask.getNumbersQueue().toArray();
			for (final Object remainingNumber : remainingNumbers)
			{
				System.out.printf("On queue :[%s]%n", remainingNumber);
			}
		}
	}

}
