package com.dizzyjock.aconex.telephone.processor;

import junit.framework.Assert;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;

public class WordMatcherQueueCheckerTest
{

	private WordMatcherQueueChecker wordMatcherQueueChecker;

	@BeforeMethod
	public void setup()
	{
		this.wordMatcherQueueChecker = new WordMatcherQueueChecker();
	}

	@Test(enabled = true)
	public void should_indicate_two_jobs_completed()
	{
		final TelephoneNumber[] queueValues = new TelephoneNumber[]
		{ new TelephoneNumber("1111"), new TelephoneNumber("2222"),
				TelephoneProcessConstants.POISON,
				TelephoneProcessConstants.POISON };

		for (int i = 0; i < queueValues.length; i++)
		{
			final boolean value = wordMatcherQueueChecker.isProcessingComplete(queueValues[i]);
			if (i == queueValues.length - 1)
			{
				Assert.assertTrue(value);
			}
			else
			{
				Assert.assertFalse(value);
			}
		}

	}

	@Test(enabled = true)
	public void should_indicate_five_jobs_completed()
	{
		final TelephoneNumber[] queueValues = new TelephoneNumber[]
		{ new TelephoneNumber("1111"),
				new TelephoneNumber("2222"),
				TelephoneProcessConstants.POISON,
				new TelephoneNumber("3333"),
				TelephoneProcessConstants.POISON,
				new TelephoneNumber("4444"),
				TelephoneProcessConstants.POISON,
				new TelephoneNumber("5555"), TelephoneProcessConstants.POISON,
				TelephoneProcessConstants.POISON };

		for (int i = 0; i < queueValues.length; i++)
		{
			final boolean value = wordMatcherQueueChecker.isProcessingComplete(queueValues[i]);
			if (i == queueValues.length - 1)
			{
				Assert.assertTrue(value);
			}
			else
			{
				Assert.assertFalse(value);
			}
		}

	}

	@Test(enabled = true)
	public void should_cope_with_exception_if_queue_empty()
	{
		final TelephoneNumber[] queueValues = new TelephoneNumber[]
		{ TelephoneProcessConstants.POISON };

		final boolean value = wordMatcherQueueChecker.isProcessingComplete(queueValues[0]);
		Assert.assertFalse(value);
	}

}
