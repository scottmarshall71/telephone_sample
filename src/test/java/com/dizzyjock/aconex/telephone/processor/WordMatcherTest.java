package com.dizzyjock.aconex.telephone.processor;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.NumberMapper;
import com.dizzyjock.aconex.telephone.data.OutputFileWriterConsoleImpl;
import com.dizzyjock.aconex.telephone.domain.MatchTask;
import com.dizzyjock.aconex.telephone.domain.Result;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberData;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberKey;
import com.dizzyjock.aconex.telephone.util.CharacterFilter;

/**
 * This class tests repeated loops through the algorithm for various conditions
 * 
 * @author ubuntu
 * 
 */
public class WordMatcherTest
{

	private WordMatcherTestFixture testFixture;

	@BeforeMethod
	public void setup()
	{
		this.testFixture = new WordMatcherTestFixture();
	}

	@Test(enabled = true)
	public void should_match_exact_length() throws InterruptedException, FileNotFoundException
	{
		testFixture.given_telephone_number_and_dictionary(new TelephoneNumber("83787277"),
				"should_match_exact_length.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test", "pass");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_match_three_number() throws InterruptedException, FileNotFoundException
	{
		testFixture.given_telephone_number_and_dictionary(new TelephoneNumber("8378997277"),
				"should_match_three_number.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_match_is_made("test", "zz", "pass");
		testFixture.then_verify_queue_is_empty();
	}

	@Test(enabled = true)
	public void should_cope_with_no_match() throws InterruptedException, FileNotFoundException
	{
		testFixture.given_telephone_number_and_dictionary(new TelephoneNumber("abcdef"),
				"should_cope_with_no_match.txt");
		testFixture.when_match_is_run();
		testFixture.then_verify_no_match_is_made();
		testFixture.then_verify_queue_is_empty();
	}

	@SuppressWarnings("unused")
	private class WordMatcherTestFixture
	{

		private WordMatcher wordMatcher;
		private final Result result;
		private final BlockingQueue<TelephoneNumberData> numbersQueue;
		private final List<MatchTask> matchTaskList = new ArrayList<>();
		private final TelephoneNumberKey key = new TelephoneNumberKey("2222");

		public WordMatcherTestFixture()
		{
			result = new Result();
			numbersQueue = new ArrayBlockingQueue<>(TelephoneProcessConstants.INITIAL_QUEUE_CAPACITY);

		}

		public void given_telephone_number_and_dictionary(final TelephoneNumber telephoneNumber, final String filename)
				throws InterruptedException, FileNotFoundException
		{
			final String fileSeparator = System.getProperty("file.separator");
			final String pathToDictionary = String.format("wordmatchertest%s%s",
					fileSeparator, filename);
			this.wordMatcher = new WordMatcher(new NumberMapper(),
					numbersQueue,
					result,
					new CharacterFilter(),
					new DictionaryFactory(pathToDictionary),
					new OutputFileWriterConsoleImpl());
			final TelephoneNumberData telephoneNumberData = new TelephoneNumberData(key, telephoneNumber);
			numbersQueue.put(telephoneNumberData);
		}

		public void when_match_is_run() throws InterruptedException
		{

			final ExecutorService execService = Executors.newFixedThreadPool(5);
			final Future<Object> future = execService.submit(wordMatcher);
			try
			{
				future.get();
			}
			catch (InterruptedException | ExecutionException e)
			{
				e.printStackTrace();
			}

			System.out.println("@@@@@@@@@@@@@@@@@@@@");
		}

		public void then_verify_match_is_made(final String... expectedMatches)
		{
			final Result actualResult = wordMatcher.getResult();

			Assert.assertEquals(actualResult.getResultMap().size(), 1);

			for (final String expectedMatch : expectedMatches)
			{
				Assert.assertTrue(actualResult.getResultMap().get(key).contains(expectedMatch));
			}

		}

		public void then_verify_queue_is_empty()
		{
			final Object[] remainingNumbers = wordMatcher.getNumbersQueue().toArray();
			for (final Object remainingNumber : remainingNumbers)
			{
				System.out.printf("On queue :[%s]%n", remainingNumber);
			}

			Assert.assertEquals(0, wordMatcher.getNumbersQueue().size());

		}

		public void then_verify_no_match_is_made()
		{
			final Result actualResult = wordMatcher.getResult();
			Assert.assertTrue(actualResult.getResultMap().isEmpty());
		}
	}

}
