package com.dizzyjock.aconex.telephone.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Utility class to produce a fresh instance of the dictionary on request.
 * 
 * @author ubuntu
 * 
 */
public class DictionaryFactory
{
	private static final String CLASSNAME = DictionaryFactory.class.getSimpleName();

	private final String filename;
	private final Set<String> lines;

	public DictionaryFactory(final String filename) throws FileNotFoundException
	{
		this.filename = filename;
		System.out.printf("[%s]:[%s] searching for file[%s]%n", Thread.currentThread().getName(),
				CLASSNAME, filename);
		final BufferedReader bufferedReader = createBufferedReader(filename);
		lines = new ConcurrentSkipListSet<>();

		try
		{
			String dictionaryWord;

			while ((dictionaryWord = bufferedReader.readLine()) != null)
			{
				if (dictionaryWord.length() > 0)
				{
					lines.add(dictionaryWord);
				}
			}

		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			closeBufferedReader(bufferedReader);
		}

	}

	private BufferedReader createBufferedReader(final String filename) throws FileNotFoundException
	{
		BufferedReader bufferedReader;
		if (isFileExternal(filename))
		{
			bufferedReader = new BufferedReader(new FileReader(filename));
			// System.out.printf("[%s]:[%s] Found external file:[%s]%n",
			// Thread.currentThread().getName(),
			// CLASSNAME, filename);
		}
		else
		{
			// assume its on the classpath
			bufferedReader =
					new BufferedReader(
							new InputStreamReader(
									Dictionary.class.getClassLoader().getResourceAsStream(filename)));
		}
		return bufferedReader;
	}

	private boolean isFileExternal(final String filename)
	{
		// check the file exists
		final File dictionaryFile = new File(filename);
		if (dictionaryFile.exists())
		{
			return true;
		}
		return false;
	}

	private void closeBufferedReader(final BufferedReader br)
	{
		try
		{
			if (br != null)
			{
				br.close();
			}
		}
		catch (final IOException ex)
		{
			ex.printStackTrace();
		}
	}

	public Dictionary createDictionary() throws FileNotFoundException
	{
		return new Dictionary(filename, lines);
	}

	public String getDictionarySrc()
	{
		return filename;

	}

	@Override
	public String toString()
	{
		return "DictionaryFactory [getDictionarySrc()="
				+ getDictionarySrc() + "]";
	}

}
