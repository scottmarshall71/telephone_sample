package com.dizzyjock.aconex.telephone.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * If time permitted, the file could be found relative to the jar.
 * <p/>
 * For speed, the path to the file is hardcoded
 */
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;

/**
 * If time permitted, the file could be found relative to the jar.
 * <p/>
 * For speed, the path to the file is hardcoded
 */
public class InputFileReaderImpl implements InputFileReader
{

	private static final String CLASSNAME = InputFileReaderImpl.class.getSimpleName();

	@Override
	public List<TelephoneNumber> readFile(final String file) throws FileNotFoundException
	{
		final List<TelephoneNumber> telephoneNumbers = new ArrayList<>();

		final File inputfile = new File(file);
		if (!inputfile.exists())
		{
			throw new FileNotFoundException("Cannnot find file at " + file);
		}

		BufferedReader br = null;

		try
		{

			String sCurrentLine;

			br = new BufferedReader(new FileReader(inputfile));

			while ((sCurrentLine = br.readLine()) != null)
			{

				System.out.printf("[%s]:[%s] read from file [%s]:[%s]%n", Thread.currentThread().getName(),
						CLASSNAME, inputfile, sCurrentLine);
				telephoneNumbers.add(new TelephoneNumber(sCurrentLine));
			}

		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			closeBufferedReader(br);
		}

		return telephoneNumbers;
	}

	private void closeBufferedReader(final BufferedReader br)
	{
		try
		{
			if (br != null)
			{
				br.close();
			}
		}
		catch (final IOException ex)
		{
			ex.printStackTrace();
		}
	}

}
