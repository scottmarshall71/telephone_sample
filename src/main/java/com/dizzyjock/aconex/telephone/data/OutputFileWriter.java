package com.dizzyjock.aconex.telephone.data;

import com.dizzyjock.aconex.telephone.domain.Result;

/**
 * Interface for different implementations to write to file
 * 
 * @author ubuntu
 * 
 */
public interface OutputFileWriter
{

	void writeFile(final Result result);
}