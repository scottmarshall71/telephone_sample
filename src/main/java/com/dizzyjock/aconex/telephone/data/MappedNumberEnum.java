package com.dizzyjock.aconex.telephone.data;

/**
 * Defines the valid input range when looking up mapped characters.
 * 
 * @author ubuntu
 * 
 */
public enum MappedNumberEnum
{

	TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, NOTMAPPED;

	public static MappedNumberEnum getMappedNumber(final String key)
	{
		switch (key)
		{
		case ("2"):
			return TWO;
		case ("3"):
			return THREE;
		case ("4"):
			return FOUR;
		case ("5"):
			return FIVE;
		case ("6"):
			return SIX;
		case ("7"):
			return SEVEN;
		case ("8"):
			return EIGHT;
		case ("9"):
			return NINE;
		default:
			return NOTMAPPED;

		}
	}

}
