package com.dizzyjock.aconex.telephone.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.dizzyjock.aconex.telephone.util.PropertyLoader;

/**
 * This class could be entirely static, but static classes are a trouble to test
 * / mock.
 * 
 * @author ubuntu
 * 
 */
public class NumberMapper
{

	private static Map<MappedNumberEnum, Set<Character>> lookup =
			new ConcurrentHashMap<MappedNumberEnum, Set<Character>>();

	private static final String CLASSNAME = NumberMapper.class.getSimpleName();

	static
	{
		final Properties props = PropertyLoader.getAllProperties();
		for (final Object key : props.keySet())
		{
			System.out.printf("[%s]:[%s] init: Key: [%s]- Value:[%s] %n", Thread.currentThread().getName(),
					CLASSNAME, key, props.get(key));
			final MappedNumberEnum mappedNumber = MappedNumberEnum.getMappedNumber(String.valueOf(key));
			if (mappedNumber == MappedNumberEnum.NOTMAPPED)
			{
				throw new RuntimeException("Unable to map " + key);
			}
			final char[] charPrimitives = ((String) props.get(key)).toCharArray();
			final Character[] charObjects = new Character[charPrimitives.length];
			for (int i = 0; i < charPrimitives.length; i++)
			{
				charObjects[i] = charPrimitives[i];
			}
			final Set<Character> charSet = new HashSet<Character>(Arrays.asList(charObjects));
			lookup.put(mappedNumber, charSet);
		}

	}

	// TODO : Maybe refactor this to throw exception ? Might be clearer signal
	public Set<Character> lookup(final MappedNumberEnum mappedNumber)
	{
		if (lookup.containsKey(mappedNumber))
		{
			return lookup.get(mappedNumber);
		}
		return Collections.<Character> emptySet();
	}
}
