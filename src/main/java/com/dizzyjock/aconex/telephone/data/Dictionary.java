package com.dizzyjock.aconex.telephone.data;

import java.util.Iterator;
import java.util.Set;

/**
 * Reads the file passed in as the constructor argument.
 * 
 * @author ubuntu
 * 
 */
public class Dictionary
{
	private static final String CLASSNAME = Dictionary.class.getSimpleName();

	private final String dictionarySrc;
	private final Iterator<String> iter;

	public Dictionary(final String dictionarySrc, final Set<String> dictionaryWords)
	{
		this.dictionarySrc = dictionarySrc;
		this.iter = dictionaryWords.iterator();
	}

	public String getDictionarySrc()
	{
		return dictionarySrc;
	}

	public String readLine()
	{
		if (!iter.hasNext())
		{
			return null;
		}
		return iter.next();
	}

	@Override
	public String toString()
	{
		return "Dictionary [getDictionarySrc()=" + getDictionarySrc() + ", readLine()=" + readLine() + "]";
	}

}
