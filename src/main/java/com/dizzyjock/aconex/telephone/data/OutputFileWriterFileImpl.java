package com.dizzyjock.aconex.telephone.data;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

import com.dizzyjock.aconex.telephone.domain.Result;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberKey;

public class OutputFileWriterFileImpl implements OutputFileWriter
{
	private static final String CLASSNAME = OutputFileWriterFileImpl.class.getSimpleName();

	@Override
	public void writeFile(final Result result)
	{
		try
		{
			if (result.getResultMap().isEmpty())
			{
				System.out.printf("[%s]:[%s] OutputFileWriter ending for with no matches%n", Thread.currentThread()
						.getName(),
						CLASSNAME);
			}

			else
			{
				for (final Map.Entry<TelephoneNumberKey, Set<String>> mapEntry : result.getResultMap().entrySet())
				{
					final String filename = mapEntry.getKey().getIdentifier() + ".output";
					final PrintWriter writer = new PrintWriter(filename, "UTF-8");
					for (final String match : mapEntry.getValue())
					{
						writer.println(match.toUpperCase());
					}
					writer.close();
					System.out.printf("[%s]:[%s] OutputFileWriter ending for[%s]%n", Thread.currentThread().getName(),
							CLASSNAME, filename);
				}
			}
		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}
	}
}
