package com.dizzyjock.aconex.telephone.data;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * If time permitted, the file could be found relative to the jar.
 * <p/>
 * For speed, the path to the file must be absolute
 */
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;

public interface InputFileReader
{
	/**
	 * For speed, the path to the file must be absolute
	 * 
	 * @param file
	 *            - the absolute path to the file.
	 * @return
	 * @throws FileNotFoundException
	 */
	public List<TelephoneNumber> readFile(final String absolutePathToFile) throws FileNotFoundException;

}
