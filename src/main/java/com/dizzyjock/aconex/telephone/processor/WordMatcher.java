package com.dizzyjock.aconex.telephone.processor;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.NumberMapper;
import com.dizzyjock.aconex.telephone.data.OutputFileWriter;
import com.dizzyjock.aconex.telephone.domain.Result;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberData;
import com.dizzyjock.aconex.telephone.util.CharacterFilter;
import com.dizzyjock.aconex.telephone.util.ThreadPoolExecutorFactory;
import com.dizzyjock.aconex.telephone.util.UncaughtExceptionHandler;

/**
 * This class takes TelephoneNumbers off the input queue, then starts the
 * processing of each number in it's own thread
 * <p/>
 * The processing of each TelephoneNumber is done in a WordMatcherTask.
 * 
 * @author ubuntu
 * 
 */
public class WordMatcher implements Callable<Object>
{

	private static final String CLASSNAME = WordMatcher.class.getSimpleName();

	private final Result result;

	/** Telephone numbers read in from files come in on the input queue. **/
	private final BlockingQueue<TelephoneNumberData> numbersQueue;

	/** Acts as controller on the queue to signal when all word is complete **/
	private final WordMatcherQueueChecker wordMatcherQueueChecker;

	private final ExecutorService executorService;
	private final static int NTHREDS = TelephoneProcessConstants.MATCHER_THREAD_POOL;
	private final DictionaryFactory dictionaryFactory;
	private final NumberMapper numberMapper;
	private final OutputFileWriter outputFileWriter;
	private final CharacterFilter characterFilter;

	public WordMatcher(final NumberMapper numberMapper,
			final BlockingQueue<TelephoneNumberData> numbersQueue,
			final Result result,
			final CharacterFilter characterFilter,
			final DictionaryFactory dictionaryFactory,
			final OutputFileWriter outputFileWriter)
	{
		this.result = result;
		this.numbersQueue = numbersQueue;
		executorService = ThreadPoolExecutorFactory.createExecutor(NTHREDS, new WordMatcherThreadFactory());
		this.dictionaryFactory = dictionaryFactory;
		this.characterFilter = characterFilter;
		this.numberMapper = numberMapper;
		this.wordMatcherQueueChecker = new WordMatcherQueueChecker();
		this.outputFileWriter = outputFileWriter;
	}

	@Override
	public Object call()
	{
		System.out.printf("[%s]:[%s] WordMatcher starting%n", Thread.currentThread().getName(),
				CLASSNAME);
		// TODO Review if these are needed
		final List<Future<Object>> futures = new ArrayList<>();

		while (true)
		{
			try
			{
				final TelephoneNumberData telephoneNumberData = numbersQueue.take();

				if (wordMatcherQueueChecker.isProcessingComplete(telephoneNumberData.getTeleponeNumber()))
				{
					break;
				}
				if (!telephoneNumberData.getTeleponeNumber().equals(TelephoneProcessConstants.POISON))
				{
					System.out.printf("[%s]:[%s] Received from queue [%s]%n", Thread.currentThread().getName(),
							CLASSNAME, telephoneNumberData.getTeleponeNumber());
					futures.add(processNumber(telephoneNumberData));
				}
			}
			catch (final InterruptedException ex)
			{
				ex.printStackTrace(System.out);
			}
			catch (final Exception e)
			{
				// need to mark one job failed, but allow others to carry on
				// with the rest
				wordMatcherQueueChecker.markFailure();

			}

		}

		System.out.printf("[%s]:[%s] WordMatcher ending%n", Thread.currentThread().getName(),
				CLASSNAME);
		executorService.shutdown();

		// write results to file here
		outputFileWriter.writeFile(result);

		return new Object();
	}

	private Future<Object> processNumber(final TelephoneNumberData telephoneNumberData) throws FileNotFoundException
	{
		System.out.printf("[%s]:[%s] passing for processing [%s]%n", Thread.currentThread().getName(),
				CLASSNAME, telephoneNumberData);
		final WordMatcherTask task = new WordMatcherTask(dictionaryFactory,
				telephoneNumberData,
				numberMapper,
				numbersQueue,
				result,
				characterFilter);

		final Future<Object> future = executorService.submit(task);
		return future;

	}

	public Result getResult()
	{
		return result;
	}

	public BlockingQueue<TelephoneNumberData> getNumbersQueue()
	{
		return numbersQueue;
	}

	/*
	 * Adding the name to the Thread makes debugging easier when reviewing logs.
	 */
	private static class WordMatcherThreadFactory implements ThreadFactory
	{

		private static final AtomicInteger THREAD_NUMBER = new AtomicInteger(1);

		@Override
		public Thread newThread(final Runnable r)
		{
			final Thread thread = new Thread(r, "word-matcher-" + (THREAD_NUMBER.getAndIncrement()));
			thread.setUncaughtExceptionHandler(new UncaughtExceptionHandler());
			return thread;
		}
	}

}
