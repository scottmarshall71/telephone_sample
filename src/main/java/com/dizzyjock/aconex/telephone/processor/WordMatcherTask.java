package com.dizzyjock.aconex.telephone.processor;

import java.io.FileNotFoundException;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;

import com.dizzyjock.aconex.telephone.data.Dictionary;
import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.MappedNumberEnum;
import com.dizzyjock.aconex.telephone.data.NumberMapper;
import com.dizzyjock.aconex.telephone.domain.MatchTask;
import com.dizzyjock.aconex.telephone.domain.Result;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberData;
import com.dizzyjock.aconex.telephone.util.CharacterFilter;

/**
 * Class to perform a character by character comparison between a
 * TelephoneNumber and a single specified word.
 * 
 * @author ubuntu
 * 
 */
public class WordMatcherTask implements Callable<Object>
{
	private static final String CLASSNAME = WordMatcherTask.class.getSimpleName();

	private final DictionaryFactory dictionaryFactory;
	private final TelephoneNumberData telephoneNumberData;
	private final CharacterFilter characterFilter;
	private final NumberMapper numberMapper;
	private final Result result;
	private final BlockingQueue<TelephoneNumberData> numbersQueue;

	public WordMatcherTask(final DictionaryFactory dictionaryFactory,
			final TelephoneNumberData telephoneNumberData,
			final NumberMapper numberMapper,
			final BlockingQueue<TelephoneNumberData> numbersQueue,
			final Result result,
			final CharacterFilter characterFilter)
	{
		this.numberMapper = numberMapper;
		this.result = result;
		this.numbersQueue = numbersQueue;
		this.characterFilter = characterFilter;
		this.dictionaryFactory = dictionaryFactory;
		this.telephoneNumberData = telephoneNumberData;
	}

	@Override
	public Object call() throws FileNotFoundException
	{
		final Dictionary dictionary = dictionaryFactory.createDictionary();

		// have the TelephoneNumber, now need to run that through the entire
		// dictionary
		String dictionaryWord;

		while ((dictionaryWord = dictionary.readLine()) != null)
		{
			if (dictionaryWord.length() > 0)
			{
				System.out.printf("[%s]:[%s] beginning matching of [%s] and [%s]%n", Thread.currentThread()
						.getName(),
						CLASSNAME, telephoneNumberData, dictionaryWord);
				final int wordStartIndex = 0;
				final int telephoneStartIndex = 0;
				final MatchTask matchTask = createMatchTask(dictionaryWord);
				match(matchTask, telephoneStartIndex, wordStartIndex, new StringBuffer());
			}
		}
		// signal this task has finished when the dictionary has been
		// scanned
		addPoisonToQueue();

		return new Object();

	}

	private MatchTask createMatchTask(final String dictionaryWord)
	{
		final String wordAfterSpecifiedCharsRemoved = characterFilter.stripPunctuation(dictionaryWord);
		final MatchTask matchTask = new MatchTask(telephoneNumberData.getTeleponeNumber(),
				wordAfterSpecifiedCharsRemoved.toLowerCase());
		return matchTask;
	}

	/**
	 * Recursive algorithm. The algorithm matches character by character the
	 * telephone number against the dictionary word.
	 * 
	 * If a match is made, Append the match to the matched characters Proceed to
	 * the next character.
	 * 
	 * If no match is made if wildcard has been used terminate else set wildcard
	 * and carry on to next character
	 * 
	 * For performance, some basic checks are performed at the start to
	 * determine whether is is worth carrying on attempting to match
	 * 
	 * @param matchTask
	 *            - data holder, of both the telephone number and the dictionary
	 *            word.
	 * @param telephoneIndex
	 *            - the index of the telephone number to be compared
	 * @param wordIndex
	 *            - the index of the dictionary word to be compared
	 * @param matchedCharacters
	 *            - any characters that have already been mapped
	 */
	private void match(final MatchTask matchTask, int telephoneIndex, int wordIndex,
			final StringBuffer matchedCharacters)
	{

		if (isWordLongerThanTelephone(matchTask))
		{
			System.out.printf("[%s]:[%s] word is longer than telephone, cannot match%n", Thread.currentThread()
					.getName(),
					CLASSNAME);
			return;
		}

		if (isWordIndexLongerThanWordBeingSearched(wordIndex, matchTask.getWord()))
		{
			System.out.printf("[%s]:[%s] reached end of word being searched[%s]%n", Thread.currentThread().getName(),
					CLASSNAME, matchTask.getWord());
			addMatchedCharactersToResult(matchedCharacters);

			addUnmatchedCharactersToQueue(matchTask, telephoneIndex);

			return;
		}

		if (isTelephoneIndexLongerThanTelephoneBeingMatched(telephoneIndex, matchTask.getTelephone()))
		{
			System.out.printf("[%s]:[%s] reached end of telephone being searched[%s]%n", Thread.currentThread()
					.getName(),
					CLASSNAME, matchTask.getTelephone());
			return;
		}

		final char telephoneNumberCharacter = matchTask.getTelephone().getTelephoneNumber().charAt(telephoneIndex);
		final char wordCharacter = matchTask.getWord().charAt(wordIndex);
		final Set<Character> characterSet = findMappedCharacters(telephoneNumberCharacter);

		if (characterSet.contains(wordCharacter))
		{
			// System.out.printf("[%s]:[%s] match on [%s] %n",
			// Thread.currentThread().getName(),
			// CLASSNAME, wordCharacter);
			// then keep on matching
			matchedCharacters.append(wordCharacter);
			match(matchTask, ++telephoneIndex, ++wordIndex, matchedCharacters);
		}
		else
		{
			if (matchTask.isWildcardUsed())
			{
				// If need to use wildcard more than once, there is no match
				// with this word, so stop here
				System.out.printf("[%s]:[%s] wildcard already used, ending [%s] now%n", Thread.currentThread()
						.getName(),
						CLASSNAME, matchTask.getWord());
			}
			else
			{
				matchTask.setWildcardUsed(true);
				System.out.printf("[%s]:[%s] setting wildcard for [%s]%n", Thread.currentThread().getName(),
						CLASSNAME, matchTask.getWord());
				// then keep on matching
				matchedCharacters.append(telephoneNumberCharacter);
				match(matchTask, ++telephoneIndex, ++wordIndex, matchedCharacters);

			}
		}
	}

	/*
	 * Returns the set of characters corresponding to the input telephone number
	 * digit e.g telephoneNumberCharacter = 3 Set<Character> returned is (d,e,f)
	 * (or whatever the current mapping is)
	 */
	private Set<Character> findMappedCharacters(final char telephoneNumberCharacter)
	{
		final String digit = String.valueOf(telephoneNumberCharacter);
		final MappedNumberEnum mappedNumber = MappedNumberEnum.getMappedNumber(digit);

		final Set<Character> characterSet = numberMapper.lookup(mappedNumber);
		return characterSet;
	}

	/*
	 * Any characters in the telephone number that have not yet been mapped are
	 * returned to the queue to see if any other words match the remainder e.g
	 * pass in the equivalent of 'rocket' if 'rock' was found in dictionary
	 * 'rock' would be matched 'et' would be returned to the queue for it's own
	 * matching run.
	 */
	private void addUnmatchedCharactersToQueue(final MatchTask matchTask, final int telephoneStartIndex)
	{
		final String remainingNumbers = matchTask.getTelephone().getTelephoneNumber().substring(telephoneStartIndex);
		if (remainingNumbers.length() > 0)
		{
			try
			{
				System.out.printf("[%s]:[%s] returning for processing [%s]%n", Thread.currentThread().getName(),
						CLASSNAME, remainingNumbers);
				final TelephoneNumberData newData = new TelephoneNumberData(
						telephoneNumberData.getImmutableKey(), new TelephoneNumber(remainingNumbers));
				numbersQueue.put(newData);
			}
			catch (final InterruptedException e)
			{
				e.printStackTrace(System.out);
			}
		}
	}

	private void addMatchedCharactersToResult(
			final StringBuffer matchedCharacters)
	{
		result.put(telephoneNumberData.getImmutableKey(), matchedCharacters.toString());

	}

	/*
	 * Used to signal this number has been matched against every word in the
	 * dictionary.
	 */
	private void addPoisonToQueue()
	{
		try
		{
			final TelephoneNumberData telephoneNumberData =
					new TelephoneNumberData(this.telephoneNumberData.getImmutableKey(),
							TelephoneProcessConstants.POISON);
			numbersQueue.put(telephoneNumberData);
		}
		catch (final InterruptedException e)
		{
			e.printStackTrace(System.out);
		}
	}

	private boolean isTelephoneIndexLongerThanTelephoneBeingMatched(final int telephoneIndex,
			final TelephoneNumber telephone)
	{
		if (telephoneIndex >= telephone.getTelephoneNumber().length())
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	private boolean isWordLongerThanTelephone(final MatchTask matchTask)
	{
		if (matchTask.getWord().length() > matchTask.getTelephone().getTelephoneNumber().length())
		{
			return true;
		}
		return false;
	}

	private boolean isWordIndexLongerThanWordBeingSearched(final int wordIndex, final String word)
	{
		return wordIndex >= word.length();
	}

	/* Left in API to facilitate unit testing */
	public Result getResult()
	{
		return result;
	}

	/* Left in API to facilitate unit testing */
	public BlockingQueue<TelephoneNumberData> getNumbersQueue()
	{
		return numbersQueue;
	}

}
