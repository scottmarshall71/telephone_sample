package com.dizzyjock.aconex.telephone.processor;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.NumberMapper;
import com.dizzyjock.aconex.telephone.data.OutputFileWriter;
import com.dizzyjock.aconex.telephone.domain.CommandLineInputData;
import com.dizzyjock.aconex.telephone.domain.Result;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberData;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumberKey;
import com.dizzyjock.aconex.telephone.util.CharacterFilter;

/**
 * Class to form the WordMatcher (the class that controls the matching process),
 * and start the WordMatcher running in it's own thread.
 * <p/>
 * Once started, it then blocks until the WordMatcher has returned.
 * 
 * @author ubuntu
 * 
 */
public class NumberProducer
{
	private static final String CLASSNAME = NumberProducer.class
			.getSimpleName();

	private final CommandLineInputData commandLineInputData;

	private final NumberMapper numberMapper;
	private final CharacterFilter characterFilter;
	private final OutputFileWriter outputFileWriter;

	public NumberProducer(final CommandLineInputData commandLineInputData,
			final NumberMapper numberMapper,
			final CharacterFilter characterFilter,
			final OutputFileWriter outputFileWriter)
	{
		this.commandLineInputData = commandLineInputData;
		this.numberMapper = numberMapper;
		this.characterFilter = characterFilter;
		this.outputFileWriter = outputFileWriter;
	}

	public void produceNumbers()
	{

		final BlockingQueue<TelephoneNumberData> numbersQueue = new ArrayBlockingQueue<>(
				TelephoneProcessConstants.INITIAL_QUEUE_CAPACITY);
		final DictionaryFactory dictionaryFactory = commandLineInputData
				.getDictionaryFactory();

		final WordMatcher wordMatcher = new WordMatcher(numberMapper,
				numbersQueue, new Result(), characterFilter, dictionaryFactory,
				outputFileWriter);

		final ExecutorService execService = Executors.newFixedThreadPool(1);
		try
		{
			// TODO If telephone numbers to be matched is greater than the
			// TelephoneProcessConstants.INITIAL_QUEUE_CAPACITY, should throw an
			// Exception.
			for (final TelephoneNumber telephoneNumber : commandLineInputData
					.getTelephoneNumbers())
			{
				final TelephoneNumberData telephoneNumberData = new TelephoneNumberData(
						new TelephoneNumberKey(telephoneNumber.getTelephoneNumber()),
						telephoneNumber);

				numbersQueue.put(telephoneNumberData);
			}

			// block here while task completes
			execService.submit(wordMatcher).get();

		}
		catch (final InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}

		finally
		{
			execService.shutdown();
			System.out.printf("[%s]:[%s] NumberProducer ending%n", Thread
					.currentThread().getName(), CLASSNAME);
		}

	}

}
