package com.dizzyjock.aconex.telephone.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;
import com.dizzyjock.aconex.telephone.data.InputFileReader;
import com.dizzyjock.aconex.telephone.domain.CommandLineInputData;
import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.exception.NoArgException;

/**
 * Reads data passed from the command line into a form that the rest of the app
 * can process
 * 
 * @author ubuntu
 * 
 */
public class CommandLineParser
{
	public static final String DICTIONARY_OPTION = "-d";
	public static final String NUMBERS_OPTION = "-n";
	public static final String FILES_OPTION = "-f";

	private static final String CLASSNAME = CommandLineParser.class.getSimpleName();

	private final InputFileReader inputFileReader;

	public CommandLineParser(final InputFileReader inputFileReader)
	{
		this.inputFileReader = inputFileReader;
	}

	public CommandLineInputData parseCommandLine(final String[] args)
			throws NoArgException, FileNotFoundException
	{
		DictionaryFactory dictionaryFactory = new DictionaryFactory(TelephoneProcessConstants.DEFAULT_DICTIONARY);
		final List<String> argList = Arrays.asList(args);
		final List<TelephoneNumber> telephoneNumbers = new ArrayList<>();
		boolean numbersFlagPassed = false;
		for (final String currentArg : argList)
		{
			if (currentArg.equals(DICTIONARY_OPTION))
			{
				System.out.printf("[%s]:[%s] Dictionary option recognized%n", Thread.currentThread().getName(),
						CLASSNAME);
				dictionaryFactory = processDictionaryArg(argList, argList.indexOf(currentArg));
			}

			if (currentArg.equals(NUMBERS_OPTION))
			{
				System.out.printf("[%s]:[%s] Number option recognized%n", Thread.currentThread().getName(),
						CLASSNAME);
				telephoneNumbers.addAll(processCommandLineNumbers(argList, argList.indexOf(currentArg)));
				numbersFlagPassed = true;
			}

			if (currentArg.equals(FILES_OPTION))
			{
				System.out.printf("[%s]:[%s] File option recognized%n", Thread.currentThread().getName(),
						CLASSNAME);
				telephoneNumbers.addAll(processFileInput(argList, argList.indexOf(currentArg)));
				numbersFlagPassed = true;
			}

		}
		if (!numbersFlagPassed)
		{
			throw new NoArgException();
		}
		return new CommandLineInputData(dictionaryFactory, telephoneNumbers);
	}

	private DictionaryFactory processDictionaryArg(final List<String> argsList, final int dictionaryFlagIndex)
			throws FileNotFoundException
	{
		final int indexOfDictionary = dictionaryFlagIndex + 1;
		if (indexOfDictionary >= argsList.size())
		{
			throw new FileNotFoundException("No dictionary argument supplied");
		}
		final String userDictionary = argsList.get(indexOfDictionary);
		if (userDictionary.length() == 0)
		{
			throw new FileNotFoundException("No dictionary argument supplied");
		}

		// check the file exists
		final File dictionaryFile = new File(userDictionary);
		if (!dictionaryFile.exists())
		{
			throw new FileNotFoundException("Could not find file at " + userDictionary);
		}

		return new DictionaryFactory(userDictionary);

	}

	private List<TelephoneNumber> processFileInput(final List<String> argsList, final int numbersFlagIndex)
			throws FileNotFoundException
	{
		final int startIndex = numbersFlagIndex + 1;
		final List<String> subList = argsList.subList(startIndex, argsList.size());
		final List<TelephoneNumber> telephoneNumbers = new ArrayList<>();
		boolean fileFound = false;
		for (final String arg : subList)
		{
			if (arg.equals(DICTIONARY_OPTION))
			{
				break;
			}
			if (arg.equals(NUMBERS_OPTION))
			{
				break;
			}
			// need to read files here
			fileFound = true;
			telephoneNumbers.addAll(inputFileReader.readFile(arg));
		}

		if (fileFound == false)
		{
			throw new FileNotFoundException("No args passed that can be processed as telephone numbers");
		}

		return telephoneNumbers;
	}

	private List<TelephoneNumber> processCommandLineNumbers(final List<String> argsList, final int numbersFlagIndex)
			throws FileNotFoundException
	{
		final int startIndex = numbersFlagIndex + 1;
		final List<String> subList = argsList.subList(startIndex, argsList.size());
		final List<TelephoneNumber> telephoneNumbers = new ArrayList<>();
		boolean numberFound = false;
		for (final String arg : subList)
		{
			if (arg.equals(DICTIONARY_OPTION))
			{
				break;
			}
			if (arg.equals(FILES_OPTION))
			{
				break;
			}
			numberFound = true;
			telephoneNumbers.add(new TelephoneNumber(arg));
		}

		if (numberFound == false)
		{
			throw new FileNotFoundException("No args passed that can be processed as telephone numbers");
		}

		return telephoneNumbers;
	}
}
