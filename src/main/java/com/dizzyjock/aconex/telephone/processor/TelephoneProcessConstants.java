package com.dizzyjock.aconex.telephone.processor;

import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;
import com.dizzyjock.aconex.telephone.util.ApplicationPropertyLoader;

/**
 * Where possible, constants that may change at some point in the future should
 * be read from a central configurable source.
 * 
 * @author ubuntu
 * 
 */
public class TelephoneProcessConstants
{
	public static final TelephoneNumber POISON = new TelephoneNumber("XXXX_XXXX");
	public static final String DEFAULT_DICTIONARY = ApplicationPropertyLoader.loadProperty("default.dictionary");
	public static final int INITIAL_QUEUE_CAPACITY = Integer.valueOf(ApplicationPropertyLoader
			.loadProperty("initial.queue.capacity"));
	public static final int MATCHER_THREAD_POOL = Integer.valueOf(ApplicationPropertyLoader
			.loadProperty("match.thread.pool"));
}
