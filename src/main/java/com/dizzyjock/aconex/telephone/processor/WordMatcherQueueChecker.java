package com.dizzyjock.aconex.telephone.processor;

import java.util.Stack;

import com.dizzyjock.aconex.telephone.domain.TelephoneNumber;

/**
 * Class to monitor how many jobs started have been completed, and signal all
 * jobs have completed
 * 
 * @author ubuntu
 * 
 */

public class WordMatcherQueueChecker
{
	private static final String CLASSNAME = WordMatcherQueueChecker.class
			.getSimpleName();

	// Ensure concurrency access on this is maintained
	private final Stack<Object> jobCounter = new Stack<>();

	public synchronized Stack<Object> getJobCounter()
	{
		return jobCounter;
	}

	/**
	 * Checks the telephone number parameters to detect any
	 * TelephoneProcessConstants.POISON return values.
	 * 
	 * When a TelephoneProcessConstants.POISON has been returned for each job
	 * started, a signal is given the processing is complete.
	 * 
	 * @param telephoneNumber
	 * @return true if all processing has finished, otherwise false
	 */
	public synchronized boolean isProcessingComplete(
			final TelephoneNumber telephoneNumber)
	{
		if (telephoneNumber.equals(TelephoneProcessConstants.POISON)
				&& !getJobCounter().isEmpty())
		{
			// System.out.printf("[%s]:[%s] POISON received%n",
			// Thread.currentThread().getName(),
			// CLASSNAME);
			getJobCounter().pop();

			if (getJobCounter().isEmpty())
			{
				System.out.printf("[%s]:[%s] Breaking processing%n", Thread
						.currentThread().getName(), CLASSNAME);
				return true;
			}

		}
		else
		{
			getJobCounter().push(new Object());

		}
		System.out.printf("[%s]:[%s] Current number of jobs in queue [%d]%n",
				Thread.currentThread().getName(), CLASSNAME, jobCounter.size());
		return false;
	}

	/**
	 * In event of an Exception being thrown, the count of active jobs must be
	 * decremented.
	 */
	public synchronized void markFailure()
	{
		if (!getJobCounter().isEmpty())
		{
			getJobCounter().pop();
		}
	}
}
