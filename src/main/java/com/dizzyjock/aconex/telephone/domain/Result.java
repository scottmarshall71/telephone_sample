package com.dizzyjock.aconex.telephone.domain;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Class to hold any successful matches between telephone number and dictionary
 * word.
 * 
 * @author ubuntu
 * 
 */
public class Result
{
	private final Map<TelephoneNumberKey, Set<String>> resultMap;

	public Result()
	{

		resultMap = new ConcurrentHashMap<>();
	}

	public void put(final TelephoneNumberKey telephoneNumberKey, final String match)
	{
		Set<String> matches;
		if (resultMap.get(telephoneNumberKey) == null)
		{
			matches = new ConcurrentSkipListSet<>();
			resultMap.put(telephoneNumberKey, matches);
		}
		else
		{
			matches = resultMap.get(telephoneNumberKey);
		}
		matches.add(match);
	}

	/**
	 * 
	 * @return unmodifiable Map containing matches
	 */
	public Map<TelephoneNumberKey, Set<String>> getResultMap()
	{
		return Collections.unmodifiableMap(resultMap);
	}

	@Override
	public String toString()
	{
		return "Result [getResultMap()=" + getResultMap() + "]";
	}

}
