package com.dizzyjock.aconex.telephone.domain;

/**
 * A unit of work for the matching algorithmn. It holds one TelephoneNumber, and
 * one one word from the dictionary. The pair are run through the matching
 * algorithmn together.
 * 
 * @author ubuntu
 * 
 */
public class MatchTask
{
	private final TelephoneNumber telephone;
	private final String word;
	private boolean wildcardUsed;

	public MatchTask(final TelephoneNumber telephone, final String word)
	{
		super();
		this.telephone = telephone;
		this.word = word;
		this.wildcardUsed = Boolean.FALSE;
	}

	public TelephoneNumber getTelephone()
	{
		return telephone;
	}

	public String getWord()
	{
		return word;
	}

	public boolean isWildcardUsed()
	{
		return wildcardUsed;
	}

	public void setWildcardUsed(final boolean wildcardUsed)
	{
		this.wildcardUsed = wildcardUsed;
	}

	@Override
	public String toString()
	{
		return "MatchTask [telephone=" + telephone + ", word=" + word + ", wildcardUsed=" + wildcardUsed + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telephone == null) ? 0 : telephone.hashCode());
		result = prime * result + (wildcardUsed ? 1231 : 1237);
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final MatchTask other = (MatchTask) obj;
		if (telephone == null)
		{
			if (other.telephone != null)
			{
				return false;
			}
		}
		else if (!telephone.equals(other.telephone))
		{
			return false;
		}
		if (wildcardUsed != other.wildcardUsed)
		{
			return false;
		}
		if (word == null)
		{
			if (other.word != null)
			{
				return false;
			}
		}
		else if (!word.equals(other.word))
		{
			return false;
		}
		return true;
	}

}
