package com.dizzyjock.aconex.telephone.domain;

public class TelephoneNumberKey
{
	public final String identifier;

	public TelephoneNumberKey(final String identifier)
	{
		this.identifier = identifier;
	}

	public String getIdentifier()
	{
		return identifier;
	}

	@Override
	public String toString()
	{
		return "TelephoneNumberKey [getIdentifier()=" + getIdentifier() + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final TelephoneNumberKey other = (TelephoneNumberKey) obj;
		if (identifier == null)
		{
			if (other.identifier != null)
			{
				return false;
			}
		}
		else if (!identifier.equals(other.identifier))
		{
			return false;
		}
		return true;
	}

}
