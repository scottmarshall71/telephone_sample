package com.dizzyjock.aconex.telephone.domain;

public class TelephoneNumberData
{
	public TelephoneNumberKey immutableKey;
	public TelephoneNumber teleponeNumber;

	public TelephoneNumberData(final TelephoneNumberKey immutableKey, final TelephoneNumber teleponeNumber)
	{
		super();
		this.immutableKey = immutableKey;
		this.teleponeNumber = teleponeNumber;
	}

	public TelephoneNumberKey getImmutableKey()
	{
		return immutableKey;
	}

	public TelephoneNumber getTeleponeNumber()
	{
		return teleponeNumber;
	}

	@Override
	public String toString()
	{
		return "TelephoneNumberData [getImmutableKey()=" + getImmutableKey() + ", getTeleponeNumber()="
				+ getTeleponeNumber() + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((immutableKey == null) ? 0 : immutableKey.hashCode());
		result = prime * result + ((teleponeNumber == null) ? 0 : teleponeNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final TelephoneNumberData other = (TelephoneNumberData) obj;
		if (immutableKey == null)
		{
			if (other.immutableKey != null)
			{
				return false;
			}
		}
		else if (!immutableKey.equals(other.immutableKey))
		{
			return false;
		}
		if (teleponeNumber == null)
		{
			if (other.teleponeNumber != null)
			{
				return false;
			}
		}
		else if (!teleponeNumber.equals(other.teleponeNumber))
		{
			return false;
		}
		return true;
	}

}
