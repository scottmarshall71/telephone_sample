package com.dizzyjock.aconex.telephone.domain;

import java.util.List;

import com.dizzyjock.aconex.telephone.data.DictionaryFactory;

/**
 * Wrapper class holding the relevent data received from the command line input.
 * 
 * @author ubuntu
 * 
 */
public class CommandLineInputData
{

	private final DictionaryFactory dictionaryFactory;
	private final List<TelephoneNumber> telephoneNumbers;

	public CommandLineInputData(final DictionaryFactory dictionaryFactory, final List<TelephoneNumber> telephoneNumbers)
	{
		super();
		this.dictionaryFactory = dictionaryFactory;
		this.telephoneNumbers = telephoneNumbers;
	}

	public DictionaryFactory getDictionaryFactory()
	{
		return dictionaryFactory;
	}

	public List<TelephoneNumber> getTelephoneNumbers()
	{
		return telephoneNumbers;
	}

	@Override
	public String toString()
	{
		return "CommandLineInputData [getDictionaryFactory()=" + getDictionaryFactory() + ", getTelephoneNumbers()="
				+ getTelephoneNumbers() + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dictionaryFactory == null) ? 0 : dictionaryFactory.hashCode());
		result = prime * result + ((telephoneNumbers == null) ? 0 : telephoneNumbers.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final CommandLineInputData other = (CommandLineInputData) obj;
		if (dictionaryFactory == null)
		{
			if (other.dictionaryFactory != null)
			{
				return false;
			}
		}
		else if (!dictionaryFactory.equals(other.dictionaryFactory))
		{
			return false;
		}
		if (telephoneNumbers == null)
		{
			if (other.telephoneNumbers != null)
			{
				return false;
			}
		}
		else if (!telephoneNumbers.equals(other.telephoneNumbers))
		{
			return false;
		}
		return true;
	}

}
