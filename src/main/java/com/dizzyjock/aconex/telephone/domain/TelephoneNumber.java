package com.dizzyjock.aconex.telephone.domain;

import com.dizzyjock.aconex.telephone.util.CharacterFilter;

/**
 * Immutable class to hold the telephone number to be matched
 * 
 * @author ubuntu
 * 
 */
public class TelephoneNumber
{
	private final String telephoneNumber;
	private final CharacterFilter characterFilter;

	/**
	 * Constructor will strip all punctuation and whitespace from the
	 * telephoneNumber arg
	 * 
	 * @param telephoneNumber
	 */
	public TelephoneNumber(final String telephoneNumber)
	{
		characterFilter = new CharacterFilter();
		this.telephoneNumber = characterFilter.stripPunctuation(telephoneNumber);
	}

	public String getTelephoneNumber()
	{
		return telephoneNumber;
	}

	@Override
	public String toString()
	{
		return "TelephoneNumber [telephoneNumber=" + telephoneNumber + "]";
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((telephoneNumber == null) ? 0 : telephoneNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final TelephoneNumber other = (TelephoneNumber) obj;
		if (telephoneNumber == null)
		{
			if (other.telephoneNumber != null)
			{
				return false;
			}
		}
		else if (!telephoneNumber.equals(other.telephoneNumber))
		{
			return false;
		}
		return true;
	}

}
