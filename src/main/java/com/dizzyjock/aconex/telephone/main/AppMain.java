package com.dizzyjock.aconex.telephone.main;

import java.io.FileNotFoundException;

import com.dizzyjock.aconex.telephone.data.InputFileReader;
import com.dizzyjock.aconex.telephone.data.InputFileReaderImpl;
import com.dizzyjock.aconex.telephone.data.NumberMapper;
import com.dizzyjock.aconex.telephone.data.OutputFileWriterFileImpl;
import com.dizzyjock.aconex.telephone.domain.CommandLineInputData;
import com.dizzyjock.aconex.telephone.exception.NoArgException;
import com.dizzyjock.aconex.telephone.processor.CommandLineParser;
import com.dizzyjock.aconex.telephone.processor.NumberProducer;
import com.dizzyjock.aconex.telephone.util.CharacterFilter;

public class AppMain
{
	private static final String CLASSNAME = AppMain.class.getSimpleName();

	public static void main(final String[] args)
	{

		for (final String arg : args)
		{
			System.out.printf("[%s]:[%s] Arg:[%s]%n", Thread.currentThread().getName(),
					CLASSNAME, arg);
		}

		final AppMain appMain = new AppMain();
		appMain.processCommandLine(args);

	}

	public void processCommandLine(final String[] args)
	{
		final long startTime = System.nanoTime();
		try
		{
			final InputFileReader inputFileReader = new InputFileReaderImpl();
			final CommandLineParser commandLineParser = new CommandLineParser(inputFileReader);
			final CommandLineInputData commandLineInputData = commandLineParser.parseCommandLine(args);
			final NumberProducer numberProducer = new NumberProducer(commandLineInputData,
					new NumberMapper(),
					new CharacterFilter(),
					new OutputFileWriterFileImpl());

			numberProducer.produceNumbers();

		}

		catch (final NoArgException e)
		{
			printUseage();
		}

		catch (final FileNotFoundException e)
		{
			System.out.printf("[%s]:[%s] %s%n", Thread.currentThread().getName(),
					CLASSNAME, e.getMessage());
			printUseage();
		}
		finally
		{
			System.out.printf("[%s]:[%s] AppMain ending%n", Thread.currentThread().getName(),
					CLASSNAME);
			final long elapsedTime = System.nanoTime() - startTime;
			final double seconds = elapsedTime / 1000000000.0;
			System.out.printf("[%s]:[%s] Time taken [%f] seconds%n", Thread.currentThread().getName(),
					CLASSNAME, seconds);
			System.exit(0);
		}

	}

	public void printUseage()
	{
		System.out.printf("[%s]:[%s] No number input detected%n", Thread.currentThread().getName(),
				CLASSNAME);
		System.out.printf("[%s]:[%s] Usage options are:%n", Thread.currentThread().getName(),
				CLASSNAME);
		System.out.printf("[%s]:[%s] -n number to pass one or more numbers directly%n", Thread.currentThread()
				.getName(),
				CLASSNAME);
		System.out.printf("[%s]:[%s] -f file to pass one or more files directly%n", Thread.currentThread().getName(),
				CLASSNAME);
		System.out.printf("[%s]:[%s] File names must be absolute paths%n", Thread.currentThread().getName(),
				CLASSNAME);
	}

}
