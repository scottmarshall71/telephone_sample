package com.dizzyjock.aconex.telephone.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Class that extracts properties from the application.properties file.
 * 
 *  @author ubuntu
 */
public class ApplicationPropertyLoader
{
	private static final String PROPERTY_FILE = "/application.properties";

	
	public static String loadProperty(final String name)
	{
		final Properties props = new Properties();
		try
		{
			props.load(PropertyLoader.class.getResourceAsStream(PROPERTY_FILE));
		} catch (final IOException e)
		{
			e.printStackTrace();
		}
		return (name == null) ? "" : props.getProperty(name);
	}
	
}
