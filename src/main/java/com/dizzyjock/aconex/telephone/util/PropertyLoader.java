package com.dizzyjock.aconex.telephone.util;

import java.io.IOException;
import java.util.Properties;

/**
 * Class that extracts properties from the mapping.properties file.
 * 
 */
public class PropertyLoader
{

	private static final String PROPERTY_FILE = "/mapping.properties";

	public static Properties getAllProperties()
	{
		final Properties props = new Properties();
		try
		{
			props.load(ApplicationPropertyLoader.class.getResourceAsStream(PROPERTY_FILE));
		}
		catch (final IOException e)
		{
			e.printStackTrace();
		}
		return props;
	}
	
	
}
