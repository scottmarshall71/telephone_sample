package com.dizzyjock.aconex.telephone.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to strip any punctuation or whitespace found from an input word.
 * 
 * @author ubuntu
 * 
 */
public class CharacterFilter
{

	private static final String PUNCTUATION_PATTERN = "\\p{Punct}";
	private static final String WHITESPACE_PATTERN = "\\p{Space}";

	public String stripPunctuation(final String value)
	{
		final StringBuffer buffer = new StringBuffer();
		for (final char c : value.toCharArray())
		{
			if (isNotToBeFiltered(c))
			{
				buffer.append(c);
			}
		}
		return buffer.toString();
	}

	private boolean isNotToBeFiltered(final char value)
	{
		return !isToBeFiltered(value);
	}

	private boolean isToBeFiltered(final char value)
	{
		boolean isToBeFiltered = Boolean.FALSE;

		final Pattern punctuationPattern = Pattern.compile(PUNCTUATION_PATTERN, Pattern.CASE_INSENSITIVE);
		final Pattern whitespacePattern = Pattern.compile(WHITESPACE_PATTERN, Pattern.CASE_INSENSITIVE);

		final List<Matcher> matchers = new ArrayList<>();
		matchers.add(whitespacePattern.matcher(String.valueOf(value)));
		matchers.add(punctuationPattern.matcher(String.valueOf(value)));
		for (final Matcher matcher : matchers)
		{
			if (matcher.find())
			{
				isToBeFiltered = Boolean.TRUE;
			}
		}

		// System.out.printf("Received into filter [%s], returning [%b]%n",
		// String.valueOf(value), isToBeFiltered);
		return isToBeFiltered;
	}

}
