package com.dizzyjock.aconex.telephone.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler
{

	@Override
	public void uncaughtException(final Thread t, final Throwable e)
	{
		System.out.printf("[%s] Caught exception, stacktrace as%n", t.getName());
		final StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		final String stacktrace = sw.toString();
		System.out.println(stacktrace);

	}

}
