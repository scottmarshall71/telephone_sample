package com.dizzyjock.aconex.telephone.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutorFactory
{
	private static final int MAX_POOL_SIZE = 20;
	private static final long KEEP_ALIVE_TIME = 0;

	public static ThreadPoolExecutor createExecutor(final int poolSize, final ThreadFactory threadFactory)
	{
		return new ThreadPoolExecutor(poolSize,
				MAX_POOL_SIZE,
				KEEP_ALIVE_TIME,
				TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(), threadFactory);
	}

}
